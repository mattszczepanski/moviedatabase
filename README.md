# movie-database
###### Mateusz Szczepański Wroclaw 13.01.2019
Simple website that allows browsing / searching movie database provided by www.omdbapi.com. Only logged in users are capable to use this feature. Users can save favourites movies into local database.


### Build locally using docker
`docker-compose build`

`docker-compose up`

`docker ps`

`docker exec moviedatabase_django_<container names trail str> python manage.py migrate`

### Build locally
#### To run backend
 `pip install -r requirements.txt`

` python manage.py migrate`

 `python manage.py runserver 8000`
   
#### To run frontend

`cd client`

`npm install`

`npm start`


#### Website is hosted on : http://localhost:3000
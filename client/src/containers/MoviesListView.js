import React from 'react';
import axios from 'axios';
import {connect} from 'react-redux';

import { Input } from 'antd';

import Movie from '../components/Movie';

const Search = Input.Search;

class MovieList extends React.Component{
  state = {
    movies:[]
  }
   handleInputChange = (value) => {

    if (this.props.token){
    axios.defaults.headers = {
        "Content-Type":"application/json",
        Authorization: this.props.token
    }

    axios.get(`http://127.0.0.1:8000/api/movies/${value}`)
    .then(res => {
      this.setState({
        movies:res.data
      });
    })}
  }
  render(){
    return(
    <div>
        <Search
         placeholder="input search text"
          onSearch={value => this.handleInputChange(value)}
           size="large"
         />
         <Movie data={this.state.movies}/>
     </div>

    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.token
  }
}


export default connect(mapStateToProps)(MovieList);

import React from 'react';
import {connect} from 'react-redux';
import axios from 'axios';
import {Card} from 'antd';


class MovieDetail extends React.Component{
  state = {
    movie:{}
  }

  componentWillReceiveProps(newProps){
    const movieTitle = this.props.match.params.movieTitle;
    if (newProps.token){
    axios.defaults.headers = {
        "Content-Type":"application/json",
        Authorization: newProps.token
    }
    axios.get(`http://127.0.0.1:8000/api/movies/details/${movieTitle}`)
    .then(res => {
      this.setState({
        movie:res.data
      });
    })
  }}
  render(){
    return(
      <Card title={this.state.movie.Title}>
      {<img alt="" src={this.state.movie.Poster} />}
          <p>
            {this.state.movie.Type} / {this.state.movie.Genre} / {this.state.movie.Released}
          </p>
          <p>
            {this.state.movie.Plot}
          </p>
      </Card>
    );
  }
}

const mapStateToProps = state => {
  return {
    token: state.token
  }
}
export default connect(mapStateToProps)(MovieDetail);

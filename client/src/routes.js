import React from 'react';
import { Route } from 'react-router-dom';

import MovieList from './containers/MoviesListView'
import MovieDetail from './containers/DetailView'
import Login from "./containers/Login";
import Signup from "./containers/Signup";

const BaseRouter = (props) =>(
  <div>
    <Route exact path="/login/" component={Login} />{" "}
    <Route exact path="/signup/" component={Signup} />{" "}

    <Route exact path='/' component={MovieList}/>
    <Route exact path='/details/:movieTitle' component={MovieDetail}/>
  </div>
);

export default BaseRouter;

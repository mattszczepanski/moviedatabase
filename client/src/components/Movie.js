import React from 'react'
import { List, Icon } from 'antd';

const IconText = ({ type, text }) => (
  <span>
    <Icon type={type} style={{ marginRight: 8 }} />
    {text}
  </span>
);

const Movie = (props) => {
  return(
    <List
    itemLayout="vertical"
    size="large"
    pagination={{
      onChange: (page) => {
        console.log(page);
      },
      pageSize: 6,
    }}
    dataSource={props.data}
    renderItem={item => (
      <List.Item
        key={item.title}
        actions={[<IconText type="star-o" text="156" />, <IconText type="like-o" text="156" />, <IconText type="message" text="2" />]}
        extra={<img width={272} alt="logo" src={item.Poster} />}
      >
        <List.Item.Meta
          title={<a href={`/details/${item.Title}`}>{item.Title}</a>}
          year={item.Year}
        />
        {item.content}
      </List.Item>
    )}
    />
  )
  }

export default Movie;

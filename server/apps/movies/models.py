from datetime import datetime

from django.db import models


class Movie(models.Model):
    title = models.CharField(max_length=120, unique=True)
    rated = models.CharField(max_length=10)
    released = models.DateField(null=True)
    runtime = models.CharField(max_length=25)
    genre = models.CharField(max_length=100)
    director = models.CharField(max_length=120)
    writer = models.CharField(max_length=400)
    actors = models.TextField()
    plot = models.TextField()
    language = models.CharField(max_length=200)
    awards = models.CharField(max_length=300)
    poster = models.URLField()
    ratings = models.TextField()
    metascore = models.IntegerField()
    imdbrating = models.DecimalField(max_digits=3, decimal_places=2)
    imdbvotes = models.DecimalField(max_digits=10, decimal_places=5)
    imdbID = models.CharField(max_length=11)
    movie_type = models.CharField(max_length=100)
    dvd = models.DateField(null=True)
    boxoffice = models.CharField(max_length=45)
    production = models.TextField()
    website = models.URLField()

    @classmethod
    def create_from_movie_title(cls, data):
        title = data['Title']
        rated = data['Rated']
        released = data['Released']
        runtime = data['Runtime']
        genre = data['Genre']
        director = data['Director']
        writer = data['Writer']
        actors = data['Actors']
        plot = data['Plot']
        language = data['Language']
        awards = data['Awards']
        poster = data['Poster']
        ratings = data['Ratings']
        metascore = int(data['Metascore'])
        imdbrating = float(data['imdbRating'].replace(',', '.'))
        imdbvotes = float(data['imdbVotes'].replace(',', '.'))
        imdbID = data['imdbID']
        movie_type = data['Type']
        dvd = data['DVD']
        boxoffice = data['BoxOffice']
        production = data['Production']
        website = data['Website']

        Movie.objects.update_or_create(
            title=title,
            released=datetime.strptime(
                released, "%d %b %Y").date(),
            runtime=runtime,
            director=director,
            genre=genre,
            writer=writer,
            actors=actors,
            language=language,
            movie_type=movie_type,
            production=production,
            defaults={
                "rated": rated,
                "plot": plot,
                "awards": awards,
                "poster": poster,
                "ratings": ratings,
                "metascore": metascore,
                "imdbrating": imdbrating,
                "imdbvotes": imdbvotes,
                "imdbID": imdbID,
                "dvd": datetime.strptime(dvd, "%d %b %Y").date(),
                "boxoffice": boxoffice,
                "website": website,
            })

    def __str__(self):
        return f'{self.title} - {self.released}'

from django.urls import path
from .views import SearchTitleView, GetMovieView, MoviesView

urlpatterns = [
    path('', MoviesView.as_view()),
    path('<str:search_phrase>/', SearchTitleView.as_view()),
    path('details/<str:movie_title>/', GetMovieView.as_view()),
]

import json

import requests
from django.conf import settings
from django.core import serializers
from rest_framework import status

from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Movie

API_KEY = settings.API_KEY
BASE_URL = f"http://www.omdbapi.com/?apikey={API_KEY}"




class OMDBBase():
    @staticmethod
    def get_data(response):
        if status.is_success(response.status_code):
            data = json.loads(response.text)
            if data['Response'] == "True":
                return data
            else:
                return {"Response": "Movie not found!"}


class MoviesView(APIView):
    def get(self, request):
        movies = Movie.objects.all()
        if len(movies) > 0:
            return Response(serializers.serialize('json', movies))
        else:
            return Response({'Search': 'No movies found!'})


class SearchTitleView(OMDBBase, APIView):
    def get(self, request, search_phrase):
        url = f'{BASE_URL}&s={search_phrase}&plot=short'
        data = self.get_data(requests.get(url)).get("Search")
        return Response(data)


class GetMovieView(OMDBBase, APIView):
    def get(self, request, movie_title):
        url = f'{BASE_URL}&t={movie_title}&plot=full'
        data = self.get_data(requests.get(url))
        try:
            Movie.create_from_movie_title(data)
        except ValueError:
            pass
        return Response(data)

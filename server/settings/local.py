from .base import *  # noqa


ADMINS = (
    ('Mateusz Szczepanski', '7szczepanski@gmail.com'),
)

MANAGERS = ADMINS

DATABASES = {
    'default': { 
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'server.db',
    }
}


# You might want to use sqlite3 for testing in local as it's much faster.
if IN_TESTING:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': '/tmp/movie-database_test.db',
        }
    }
